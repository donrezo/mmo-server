package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
	"playground/proj/AuthServer/config"
	"playground/proj/AuthServer/src/Controller/Local"
	"playground/proj/AuthServer/src/Proto/authorization"
)

func main()  {

	config.LoadConfigs()

	lis, err := net.Listen("tcp", ":50051")

	if err != nil {
		log.Fatalf("Error occured: %v", err)
	}

	s := grpc.NewServer()

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to server: %v", err)
		}
	}()

	authpb.RegisterAuthServiceServer(s, &Local.Grpc{})

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch

	fmt.Println("Stopping server")
	s.Stop()
	lis.Close()
}






