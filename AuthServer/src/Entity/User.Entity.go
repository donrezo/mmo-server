package Entity

import (
	"fmt"
	"time"
)

type User struct {
	Id int `gorm:"primary_key, AUTO_INCREMENT"`
	Login string `gorm:"type:unique"`
	Password string
	Token Token `gorm:"foreignkey:UserId"`
	Active bool
	Created time.Time
}

func (user *User) TableName() string{
	return "users"
}

func (user User) ToString() string{
	return fmt.Sprintf(" Id: %d\n Login: %s\n Active: %t\n", user.Id,user.Login,user.Active)
}
