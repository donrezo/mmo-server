package Entity

type Token struct {
	UserId uint
	Token string `gorm:"type:unique"`
}

func (user *Token) TableName() string{
	return "users_tokens"
}