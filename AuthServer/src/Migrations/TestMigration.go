package Migrations

import (
	"fmt"
	"playground/proj/AuthServer/config/DB"
	"playground/proj/AuthServer/src/Entity"
)

func Migrate()  {
	db , err := DB.GetDB()
	if err != nil {
		fmt.Println(err)
	}
	db.AutoMigrate(&Entity.User{}, Entity.Token{})
	fmt.Println("Jeblo")
}
