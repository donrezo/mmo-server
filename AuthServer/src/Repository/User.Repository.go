package Repository

import (
	"playground/proj/AuthServer/config/DB"
	"playground/proj/AuthServer/src/Entity"
	"time"
)

type UserRepository struct {
}

func (repo UserRepository) FindAll() ([]Entity.User, error) {
	db, err := DB.GetDB()

	if err != nil {
		return nil, err
	} else {
		var users []Entity.User
		db.Find(&users)
		return users, nil
	}
}

func (repo UserRepository) FindById(Id int) (Entity.User,error){
	db, err := DB.GetDB()

	if err != nil {
		return Entity.User{}, err
	} else {
		var User Entity.User
		db.Where("id = ?", Id).First(&User)
		return User, nil
	}
}

func (repo UserRepository) GetUserTokenBy(login string, pass string) (string,error){
	db, err := DB.GetDB()

	if err != nil {
		return "", err
		} else {
		var User Entity.User
		db.Where("login = ? && password = ?", login,pass).Preload("Token").First(&User)

		return User.Token.Token, nil
	}
}


func (repo UserRepository) CreateUser(login string, password string) {
	db, err := DB.GetDB()

	user := Entity.User{Login:login,Password:password,Token: Entity.Token{Token:"fjsdfhsdkjfhksdj"},Active:true,Created:time.Now()}

	if err != nil {} else {
		db.Create(&user)
	}
}