package config

import "github.com/joho/godotenv"

func LoadConfigs()  {
	godotenv.Load("config/settings.env")
	godotenv.Load("config/addresses.env")
}
