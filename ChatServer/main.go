package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"os/signal"
)

func main()  {
	lis, err := net.Listen("tcp", ":50051")

	if err != nil {
		log.Fatalf("Error occured: %v", err)
	}

	s := grpc.NewServer()

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to server: %v", err)
		}
	}()



	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch

	fmt.Println("Stopping server")
	s.Stop()
	lis.Close()
}
