package main

import (
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"log"
	"net"
	"os"
	"os/signal"
	"playground/proj/AuroraFrontServer/src/Controller/Local"
	"playground/proj/AuroraFrontServer/src/Proto/communication"
	"time"
)



func main()  {

	lis, err := net.Listen("tcp", ":50052")

	if err != nil {
		log.Fatalf("Error occured: %v", err)
	}

	s := grpc.NewServer(grpc.KeepaliveParams(keepalive.ServerParameters{
		MaxConnectionAgeGrace: time.Second * 3,
		Time: time.Second *1}), grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{MinTime: time.Second * 10}))

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to server: %v", err)
		}
	}()

	communicationpb.RegisterCommunicationServiceServer(s, &Local.Grpc{})

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)
	<-ch


	fmt.Println(" Stopping server ")
	s.Stop()
	lis.Close()

}

