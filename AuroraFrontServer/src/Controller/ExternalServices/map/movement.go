package _map

import (
	"context"
	"fmt"
	"io"
	"playground/proj/AuroraFrontServer/config"
	"playground/proj/AuroraFrontServer/src/Hub"
	"playground/proj/AuroraFrontServer/src/Proto/map"
	"time"
)

func StreamObjects(c chan Hub.Message, token string, ctx context.Context)  {

	client := config.InitGrpcClients().MapClient

	id := LoadOnMap(c, token)

	call, err := client.Call(ctx)
	call.Send(&movepb.RequestObjects{Id:&movepb.Id{Id:token}})

	if err != nil {
		fmt.Println(err)
	}

	go func() {
		select {
		case <-ctx.Done():
			ctx2, cancel := context.WithTimeout(context.Background(), 30 * time.Second)
			client.Stop(ctx2, &movepb.Id{Id:token})
			cancel()
			return
		}
	}()

	for {
		in, err := call.Recv()

		if err == io.EOF {
			fmt.Println("end")
			break
		}

		if err != nil {
			fmt.Println("cosinnefo")
			break
		}

		//to nie dziala many, potrzebny inny sposob na stringi

		msg := Hub.Message{Type: "Object",Action:"DiscoverObject",Value:in.Id+","+in.Position.Postion}

		c <- msg

	}

	fmt.Println(id)

	if err != nil {
		fmt.Println(err)
	}


}

func LoadOnMap(c chan Hub.Message, id string) string{

	client := config.InitGrpcClients().MapClient
	ctx := context.Background()

	res, err := client.Start(ctx,&movepb.Id{Id:id})
	if err != nil {
		fmt.Println(err)
	}

	//send to channel
	Id := res.GetId()

	//send to channel
	Pos:= res.GetPosition()

	msg := Hub.Message{Type: "Init",Value:Pos.Postion}

	c <- msg

	return Id
}

func MoveCharacter(id string, value string, c chan Hub.Message)  {
	client := config.InitGrpcClients().MapClient
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)

	fmt.Println(id)

	val := 2

	for x := 0; x < val ; x++ {
		r, err := client.MoveChar(ctx, &movepb.Command{Id: id, Move: &movepb.Move{Direction: value}})
		fmt.Println(r.GetPosition().Postion)
		if err != nil {
			fmt.Println(err)
		}

		status := r.GetResponse()
		if status != nil {
			msg := Hub.Message{Type: "Status",Value: status.Status}
			c <- msg
		}
		pos := r.GetPosition()

		if pos != nil {

			msg := Hub.Message{Type: "Map",Action:"MoveResponse",Value: pos.Postion}
			c <- msg
		}
	}

	cancel()
}

func DisconnectFromMap(token string)  {
	client := config.InitGrpcClients().MapClient
	ctx := context.Background()

	res, err := client.Stop(ctx, &movepb.Id{Id:token})

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(res.Status)
}

