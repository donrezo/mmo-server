package authorization

import (
	"context"
	"fmt"
	"playground/proj/AuroraFrontServer/src/Proto/authorization"
	"playground/proj/AuroraFrontServer/config"
)

func Auth(login string, password string) string{
	client := config.InitGrpcClients().AuthClient
	ctx := context.Background()
	res, err := client.Auth(ctx,&authpb.Credidentals{Login: login, Password: password})

	if err != nil {
		fmt.Println(err)
	}

	Token := res.GetToken()

	if Token != nil {
		return  Token.Token
	}

	return ""
}
