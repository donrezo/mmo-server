package Local

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"io"
	"playground/proj/AuroraFrontServer/src/Controller/ExternalServices/authorization"
	"playground/proj/AuroraFrontServer/src/Controller/ExternalServices/map"
	"playground/proj/AuroraFrontServer/src/Hub"
	"playground/proj/AuroraFrontServer/src/Proto/communication"
	"strings"
)

type Grpc struct
{}

var pool = *Hub.NewPool()

func (*Grpc) Connect(stream communicationpb.CommunicationService_ConnectServer) error {
	Message := make(chan Hub.Message)
	ctx, cancel := context.WithCancel(context.Background())
	Connected := false
	Token := ""
	Id, err := uuid.NewRandom()
	if (err != nil){
		return nil
	}
	

	go func() {
		for Msg := range Message {
			sendErr := stream.Send(&communicationpb.Response{Type:Msg.Type,Response:Msg.Value})
			if sendErr != nil{
				fmt.Println("ERROR WITH STREAM SEND FOR: ",Token," Error message: ", sendErr)
				cancel()
				pool.Delete(Token)
				break
			}
		}
	}()

	for {
		msg, err := stream.Recv()

		if err != nil {
			fmt.Println("ERROR WITH STREAM RECEIVE FOR: ",Token," Error message: ", err)
			cancel()
			pool.Delete(Token)
			break
		}
		if err != io.EOF {
			fmt.Println("eof")
		}

		Type := msg.GetType()
		Action := msg.GetAction()
		Value := msg.GetRequest()

		if Connected == true {
			 MessageHandler(Type, Action, Value, Id.String(), Message)
		}

		if Connected == false {
			if Type == "Auth" {
				creds := strings.Split(Value, ",")
				if token := authorization.Auth(creds[0],creds[1]);
				token == "" {
					return nil
				} else {
					Message <- Hub.Message{Type:Type,Action:"TokenReceived",Value: token}
					Token = token
					pool.Add(Id.String(), Hub.Connections{ID: Token, CommunicationChannel: Message})
					go OpenStreams(Id.String(),Message, ctx)
				}

				Connected = true

			} else {
				return nil
			}
			}
		}
	return nil
}

func OpenStreams(id string, c chan Hub.Message, ctx context.Context)  {
	 _map.StreamObjects(c,id,ctx)
}

func MessageHandler(Type string, Action string, Value string, Id string, c chan Hub.Message){
	switch Type {
	case "Auth":
		switch Action {
		case "Auth":
			authorization.Auth("login", "password")
		}
	case "Map":
		switch Action {
		case "MoveRequest":
			_map.MoveCharacter(Id, Value, c)
		}
	case "Game":
		switch Action {
		case "AttackRequest":
			fmt.Println("bb")
		case "InventoryRequest":
			fmt.Println("bb")
		case "SkillRequest":
			fmt.Println("bb")
		}

	case "Market":
		switch Action {
		case "Item":
			fmt.Println("bb")
		}
	case "Chat":
		switch Action {
		case "All":
			fmt.Println("bb")
		}
	}


}




