package Hub

import (
	"sync"
)

type Pool struct {
	Mutex sync.RWMutex
	Pool map[string]Connections
}

type Message struct {
	Type string
	Action string
	Value string
}

type Connections struct {
	ID string
	CommunicationChannel chan Message
}

func NewPool() *Pool {
	return &Pool{Mutex: sync.RWMutex{}, Pool: make(map[string]Connections)}
}

func (p *Pool) Add(key string, value Connections) {
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	p.Pool[key] = value
}

func (p *Pool) Delete(key string){
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	delete(p.Pool,key)
}

func (p *Pool) Get(key string) Connections{
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	v := p.Pool[key]

	return v
}

func (p *Pool) Modify(key string, conn Connections) {
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	delete(p.Pool,key)
	p.Pool[key] = conn
}



