package config

import (
	"google.golang.org/grpc"
	"log"
	"playground/proj/AuroraFrontServer/src/Proto/authorization"
	"playground/proj/AuroraFrontServer/src/Proto/map"
)


type GrpcClients struct {
	AuthClient authpb.AuthServiceClient
	MapClient movepb.MoveServiceClient
}

func initclient(address string)  *grpc.ClientConn {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	return conn
}

func moveServiceClient() movepb.MoveServiceClient {

	conn := initclient("localhost:50054")
	c := movepb.NewMoveServiceClient(conn)

	return c
}

func authServiceClient() authpb.AuthServiceClient  {
	conn := initclient("localhost:50051")
	c := authpb.NewAuthServiceClient(conn)

	return c
}

func InitGrpcClients() GrpcClients{
	AuthClient := authServiceClient()
	MapClient := moveServiceClient()
	return GrpcClients{AuthClient: AuthClient, MapClient: MapClient}
}

