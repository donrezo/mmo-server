package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"playground/proj/proto"
)

func main()  {

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	c := communicationpb.NewCommunicationServiceClient(conn)

	ctx := context.Background()

	req, err := c.Disconnect(ctx, &communicationpb.CommandRequest{})
	if err!=nil {
		log.Fatal(err)
	}
	fmt.Println(req.Response)

	w := make(chan string)
	<-w

}
