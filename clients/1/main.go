package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
	"playground/proj/clients/1/proto"
	"time"
)

func main()  {

	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := communicationpb.NewCommunicationServiceClient(conn)
	for i := 0; i < 1; i++ {
		start := time.Now()
		stream, err := c.Connect(context.Background())

		if senderr := stream.Send(&communicationpb.Request{Type:"Auth",Action:"AuthRequest",Request:"admin,admin"});
		senderr != nil {
			fmt.Println(senderr)
		}

		fmt.Println("wyslano")

		if err != nil {
			log.Fatal(err)
		}

		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				break
			}

			fmt.Println(time.Since(start).Seconds(), " : ", msg.GetAction(), msg.GetType(), msg.GetResponse())
		}
	}
	fmt.Println("SPADAM")
}
