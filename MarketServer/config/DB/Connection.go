package DB

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
)

func GetDB() (*gorm.DB, error)  {
	dbDriver := os.Getenv("DBDRIVER")
	user := os.Getenv("DBUSER")
	pass := os.Getenv("DBPASS")
	dbname := os.Getenv("DBNAME")

	db, err := gorm.Open(dbDriver, user+":"+pass+"@/"+dbname+"?charset=utf8&parseTime=true")
	if err != nil {
		return nil, err
	}
	return db, nil
}
