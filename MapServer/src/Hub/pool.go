package Hub

import (
	"playground/proj/MapServer/src/Navmesh"
	"sync"
)

type Pool struct {
	Mutex sync.RWMutex
	Pool map[string]Navmesh.Player
}


func NewPool() *Pool {
	return &Pool{Mutex: sync.RWMutex{}, Pool: make(map[string]Navmesh.Player)}
}

func (p *Pool) Add(key string, value Navmesh.Player) {
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	p.Pool[key] = value
}

func (p *Pool) Delete(key string){
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	delete(p.Pool,key)
}

func (p *Pool) Get(key string) Navmesh.Player{
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	v := p.Pool[key]

	return v
}

func (p *Pool) Modify(key string, player Navmesh.Player) {
	defer p.Mutex.Unlock()
	p.Mutex.Lock()
	delete(p.Pool,key)
	p.Pool[key] = player
}
