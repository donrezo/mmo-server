package Local

import (
	"context"
	"fmt"
	"playground/proj/MapServer/src/Hub"
	"playground/proj/MapServer/src/Navmesh"
	"playground/proj/MapServer/src/Proto/map"
	"time"
)

var Pool = *Hub.NewPool()

type Grpc struct
{}

func (*Grpc) Start(ctx context.Context,req *movepb.Id) (*movepb.Object, error) {



	//load from db
	Pr := Navmesh.NewPlayerMap(req.Id,2,3,4,5,6,7)
	Pool.Add(req.Id, Pr)

	res := &movepb.Object{Id:Pr.UUID,
		Position:&movepb.Position{Postion:Pr.ToString()}}

	return res, nil
}

func (*Grpc) Stop(ctx context.Context,req *movepb.Id) (*movepb.StatusResponse, error) {
	fmt.Println("STOP", req.Id)
	CharID := req.GetId()
	Pool.Delete(CharID)
	res := &movepb.StatusResponse{}

	return res, nil
}

func (*Grpc) MoveChar(ctx context.Context,req *movepb.Command) (*movepb.MoveResponse, error) {
	CharID := req.GetId()
	CharCommand := req.GetMove().Direction
	Char := Pool.Get(CharID)
	NewPostion := Char.Move(CharCommand)

	Pool.Modify(CharID,NewPostion)

	fmt.Println(Pool.Pool)
	res := &movepb.MoveResponse{MoveRes:&movepb.MoveResponse_Position{Position: &movepb.Position{}}}

	return res, nil
}


func (*Grpc) Kick(ctx context.Context, req *movepb.KickRequest) (*movepb.KickResponse, error) {

	return &movepb.KickResponse{}, nil
}

func (*Grpc) Call(stream movepb.MoveService_CallServer) error {

	ctx, cancel := context.WithCancel(context.Background())
	call, err:= stream.Recv()

	if err != nil{
		fmt.Println(err)
	}

	CharID := call.Id.Id

	fmt.Println(CharID)

	Obj := make(chan Navmesh.Player)

	go ListenOnPool(CharID, Obj, ctx)

	for Objects := range Obj {

		fmt.Println("SENDING:", Pool.Pool)
		msg := &movepb.Object{
			Id: Objects.UUID,
			Position: &movepb.Position{
				Postion:Objects.ToString()}}

		err := stream.Send(msg)
		if err != nil {
			fmt.Println(err)
			Pool.Delete(CharID)
			stream.Context().Done()
			cancel()
			break
		}
	}

	return nil
	// request all objects by char id, open stream for char id with objects around his position
}



func ListenOnPool(CharID string, c chan Navmesh.Player, ctx context.Context)  {
	ticker := time.NewTicker(1*time.Second)
	flag := 0


	for second := range ticker.C {
		go func() {
			select {
			case <- ctx.Done():
				ticker.Stop()
				return
			}
		}()

		if flag == 0 {
			flag = 1

			CharPos := Pool.Get(CharID)

			fmt.Println(second, CharPos)
			if len(CharPos.UUID) == 0 {
				break
			}

			for k, v := range Pool.Pool {

				if k != CharID {
					opponent := [2]int{v.X,v.Y}
					DistanceX := CheckDistance(CharPos.X, opponent[0])
					DistanceY := CheckDistance(CharPos.Y, opponent[1])
					Distance := DistanceX - DistanceY

					fmt.Println("DISTANCE:", Distance)

					if Distance < 20 {
						c <- Navmesh.Player{v.UUID, v.X, v.Y, v.Z, v.RX, v.RY, v.RZ}
					}

				}
			}

			flag = 0
		}
	}
	ticker.Stop()


}

func (*Grpc) Calculate(ctx context.Context, req *movepb.CheckDistance) (*movepb.ResponseDistance, error) {
	Id := req.GetId().Id
	Id2 := req.GetId2().Id

	Object1 := Pool.Get(Id)
	Object2 := Pool.Get(Id2)

	DistanceX := CheckDistance(Object1.X, Object2.X)
	DistanceY := CheckDistance(Object1.Y, Object2.Y)
	Distance := CheckDistance(DistanceX,DistanceY)

	//modify distance proto

	res := &movepb.ResponseDistance{Id: &movepb.Id{Id: Id},Id2:&movepb.Id{Id:Id2},Value:int32(Distance)}

	return res, nil
	}

func CheckDistance(x int, y int) int{

	if x >= y {
		return x - y
	} else {
		return y - x
	}
}
