package Navmesh

import (
	"fmt"
	"strconv"
	"strings"
)

type Player struct {
	UUID string
	X int
	Y int
	Z int
	RX int
	RY int
	RZ int
}

type Position struct {
	x int
	y int
	z int
	rx int
	ry int
	rz int
}

func NewPlayerMap(UUID string,x int, y int, z int, rx int, ry int, rz int) Player{
	return Player{UUID,x,y,z,rx,ry,rz}
}

func (p *Player) Move(Command string) Player{

	UUID := p.UUID
	x := p.X
	y := p.Y
	z := p.Z
	rx := p.RX
	ry := p.RY
	rz := p.RZ


	switch Command {
	case "UP":
		x++
	case "DOWN":
		x--
	case "RIGHT":
		y++
	case "LEFT":
		y--
	case "UPRIGHT":
		x++
		ry++
	case "UPLEFT":
		x++
		rx++
	case "DOWNRIGHT":
		x--
		ry--
	case "DOWNLEFT":
		x--
		rx--
	}

	return Player{UUID,x,y,z,rx,ry, rz}
}

func (p *Player) Position() Position{
	return Position{p.X, p.Y,p.Z,p.RX, p.RY, p.RZ}
}

func (p *Player) ToString() string{
	a := []string{strconv.Itoa(p.X), strconv.Itoa(p.Y),strconv.Itoa(p.Z),strconv.Itoa(p.RX), strconv.Itoa(p.RY), strconv.Itoa(p.RZ)}

	b := strings.Join(a,",")

	fmt.Println("OTO A: ",a, "oto B: ", b)
	return b
}
